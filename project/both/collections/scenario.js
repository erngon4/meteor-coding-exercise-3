Scenario = new Mongo.Collection('scenario');

Scenario.attachSchema(new SimpleSchema({

  title:{
    type:String
  },
  scenarioDescription:{
    type:String
  },
  characterLevel:{
    type:Number
  },
  npCharacterIds:{
    type:[String],
    optional: true
  }
  
}));

/*
 * Add query methods like this:
 *  Scenario.findPublic = function () {
 *    return Scenario.find({is_public: true});
 *  }
 */